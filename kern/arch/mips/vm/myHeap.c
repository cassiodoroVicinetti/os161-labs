// include copiati da ram.c
#include <types.h>
#include <lib.h>
#include <vm.h>
#include <mainbus.h>

#include "myAlloc.h"
#include "myList.h"
#include "myHeap.h"

typedef struct partizione_s {
  void *base;
  size_t limit;
  char type; // 0 = free, 1 = allocated
} partizione_t;

struct heap_s {
  void *memory;
  size_t size;
  heap_policy_t policy;
  tipoLista partitionList;
};

void freePartition(void *dato);
int confrontaPartizione(void *dato, void *id);

heap_t *heapCreate (size_t heapSize, paddr_t *firstpaddr) {
  heap_t *h;
  partizione_t *nuova;
  paddr_t firstpaddr_backup;
  
  firstpaddr_backup = *firstpaddr;
  myAllocInit(firstpaddr, &heapSize);
  
  h = (heap_t*)myAllocMalloc(sizeof(heap_t));
  h->size = heapSize;
  
  h->memory = (void*)0; // indirizzo relativo a MIPS_KSEG0
  h->policy = best_fit;
  
  Lista_Init(&(h->partitionList));
  
  // Partizione che era già occupata prima di ram_bootstrap()
  nuova = (partizione_t*)myAllocMalloc(sizeof(partizione_t));
  nuova->base = h->memory;
  nuova->limit = (size_t)((void*)firstpaddr_backup - (void*)h->memory);
  nuova->type = 1;
  Lista_Insert(h->partitionList, nuova);
  
  // Partizione riservata alla struttura heap_t
  nuova = (partizione_t*)myAllocMalloc(sizeof(partizione_t));
  nuova->base = (void*)firstpaddr_backup;
  nuova->limit = (size_t)((void*)(*firstpaddr) - (void*)firstpaddr_backup);
  nuova->type = 1;
  Lista_Insert(h->partitionList, nuova);
  
  nuova = (partizione_t*)myAllocMalloc(sizeof(partizione_t));
  nuova->base = (void*)(*firstpaddr);
  nuova->limit = h->size - (size_t)((void*)(*firstpaddr));
  nuova->type = 0;
  Lista_Insert(h->partitionList, nuova);
  
  return h;
}

void heapDestroy (heap_t *h) {
  // WARNING mai chiamata
  Lista_Free(h->partitionList, &freePartition);
  //myAllocFree(h->memory);
  myAllocFree((void*)h);
}

void *heapAlloc (heap_t *h, size_t size) {
  char found;
  void *nodoFound, *nodoCorrente;
  partizione_t *partizioneFound, *nuovaPartizione, *partizioneCorrente;
  
  found = 0;
  switch(h->policy) {
    case first_fit:
      nodoFound = Lista_GetHead(h->partitionList);
      do {
	partizioneFound = (partizione_t*)Lista_GetDato(nodoFound);
	if (partizioneFound->type == 0 && partizioneFound->limit >= size)
	  found = 1;
	else
	  nodoFound = Lista_NextNodo(nodoFound);
      } while (found == 0 && nodoFound != NULL);
      break;
      
    case best_fit:
      nodoCorrente = Lista_GetHead(h->partitionList);
      do {
	partizioneCorrente = (partizione_t*)Lista_GetDato(nodoCorrente);
	if (partizioneCorrente->type == 0 && partizioneCorrente->limit >= size) {
	  if (found == 0) {
	    // È la prima partizione libera che si è trovata
	    found = 1;
	    nodoFound = nodoCorrente;
	    partizioneFound = partizioneCorrente;
	  } else if (partizioneCorrente->limit < partizioneFound->limit) {
	    // La partizione appena trovata è più piccola della partizione finora trovata
	    nodoFound = nodoCorrente;
	    partizioneFound = partizioneCorrente;
	  }
	}
	nodoCorrente = Lista_NextNodo(nodoCorrente);
      } while (nodoCorrente != NULL);
      break;
      
    case worst_fit:
      nodoCorrente = Lista_GetHead(h->partitionList);
      do {
	partizioneCorrente = (partizione_t*)Lista_GetDato(nodoCorrente);
	if (partizioneCorrente->type == 0 && partizioneCorrente->limit >= size) {
	  if (found == 0) {
	    // È la prima partizione libera che si è trovata
	    found = 1;
	    nodoFound = nodoCorrente;
	    partizioneFound = partizioneCorrente;
	  } else if (partizioneCorrente->limit > partizioneFound->limit) {
	    // La partizione appena trovata è più grande della partizione finora trovata
	    nodoFound = nodoCorrente;
	    partizioneFound = partizioneCorrente;
	  }
	}
	nodoCorrente = Lista_NextNodo(nodoCorrente);
      } while (nodoCorrente != NULL);
      break;
  }
  
  if (found == 0) {
    kprintf("WARNING heapAlloc(): Nessuna partizione libera è grande abbastanza!\n");
    return 0;
  }
    
  if (partizioneFound->limit > size) {
    nuovaPartizione = (partizione_t*)myAllocMalloc(sizeof(partizione_t));
    nuovaPartizione->type = 0;
    nuovaPartizione->base = partizioneFound->base + size;
    nuovaPartizione->limit = partizioneFound->limit - size;
    Lista_InsertAfter(h->partitionList, nodoFound, nuovaPartizione);
    partizioneFound->limit = size;
  }
  partizioneFound->type = 1;
  return partizioneFound->base;
}

void heapFree (heap_t *h, void *p) {
  void *nodoCorrente, *nodoPrec, *nodoSucc;
  partizione_t *partizionePrec, *partizioneCorrente, *partizioneSucc;
  
  nodoCorrente = Lista_NodoSearch(h->partitionList, p, &confrontaPartizione);
  partizioneCorrente = (partizione_t*)Lista_GetDato(nodoCorrente);
  partizioneCorrente->type = 0;
  
  kprintf("heapFree(): Ho liberato %d byte\n", partizioneCorrente->limit);
  
  // Controlla la partizione precedente e se libera mergia
  nodoPrec = Lista_PrevNodo(nodoCorrente);
  if (nodoPrec != NULL) {
    partizionePrec = (partizione_t*)Lista_GetDato(nodoPrec);
    if (partizionePrec->type == 0) {
      partizioneCorrente->limit += partizionePrec->limit;
      partizioneCorrente->base = partizionePrec->base;
      Lista_NodoDelete(h->partitionList, nodoPrec, &freePartition);
    }
  }
  
  // Controlla la partizione successiva e se libera mergia
  nodoSucc = Lista_NextNodo(nodoCorrente);
  if (nodoSucc != NULL) {
    partizioneSucc = (partizione_t*)Lista_GetDato(nodoSucc);
    if (partizioneSucc->type == 0) {
      partizioneCorrente->limit += partizioneSucc->limit;
      Lista_NodoDelete(h->partitionList, nodoSucc, &freePartition);
    }
  }
}

void heapSetPolicy (heap_t *h, heap_policy_t policy) {
  // WARNING mai chiamata
  if (h != NULL)
    h->policy = policy;
}

void freePartition(void *dato) {
  myAllocFree((void*)((partizione_t*)dato));
}

int confrontaPartizione(void *dato, void *id) {
  partizione_t *partizione;
  partizione = (partizione_t*)dato;
  return (partizione->base - id);
}
