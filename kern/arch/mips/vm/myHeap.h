#ifndef _MYHEAP_INCLUDED
#define _MYHEAP_INCLUDED

typedef enum {
  first_fit,
  best_fit, // predefinita
  worst_fit
} heap_policy_t;

typedef struct heap_s heap_t;

heap_t *heapCreate (size_t heapSize, paddr_t *firstpaddr);
void heapDestroy (heap_t *h);
void *heapAlloc (heap_t *h, size_t size);
void heapFree (heap_t *h, void *p);
void heapSetPolicy (heap_t *h, heap_policy_t policy);

#endif
