#ifndef _MYALLOC_INCLUDED
#define _MYALLOC_INCLUDED

void myAllocInit(paddr_t *firstpaddr, size_t *heapSize);
paddr_t myAllocMalloc(size_t size);
void myAllocFree(void *addr);

#endif
