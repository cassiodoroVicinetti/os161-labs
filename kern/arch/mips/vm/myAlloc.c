// include copiati da ram.c
#include <types.h>
#include <lib.h>
#include <vm.h>
#include <mainbus.h>

#include "myAlloc.h"

#define MYHEAPMAXSIZE PAGE_SIZE * 3 // PAGE_SIZE = 4096 byte

static paddr_t firstpaddr_myHeap;
static paddr_t lastpaddr_myHeap;

void myAllocInit(paddr_t *firstpaddr, size_t *heapSize) {
	firstpaddr_myHeap = *firstpaddr;
	lastpaddr_myHeap = *firstpaddr + MYHEAPMAXSIZE - 1;
	*firstpaddr = lastpaddr_myHeap + 1;
	*heapSize -= MYHEAPMAXSIZE;
}

paddr_t myAllocMalloc(size_t size) {
	paddr_t paddr;

	if (firstpaddr_myHeap + size > lastpaddr_myHeap)
	// TODO
	  kprintf("WARNING myAllocMalloc(): I MYHEAPMAXSIZE byte della struttura heap_t sono pieni -> out of memory\n");

	paddr = firstpaddr_myHeap;
	firstpaddr_myHeap += size;

	return PADDR_TO_KVADDR(paddr);
}

void 
myAllocFree(void*addr)
{
	// TODO
	kprintf("WARNING myAllocFree(): nothing - leak the memory.\n");

	(void)addr;
}
