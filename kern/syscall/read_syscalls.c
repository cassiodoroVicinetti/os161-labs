#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <lib.h>
#include <uio.h>
#include <syscall.h>
#include <vnode.h>
#include <vfs.h>
#include <current.h>

#define BUFFER_SIZE 21 // '\0' compreso -> l'utente può digitare fino a 20 caratteri in una sola volta

int sys_read(userptr_t buffer, int nBytes, int *retval) {
  // WARNING Assumo che nBytes sia sempre uguale a 1.
  
  static char bufferStatico[BUFFER_SIZE];
  static int index = -1;
  char *bufferCastato;
  
  if (index == -1 || bufferStatico[index] == '\0') {
    // Acquisisci altri caratteri.
    index = -1;
    kgets(bufferStatico, BUFFER_SIZE);
  }
  
  index++;
  bufferCastato = (char*)buffer;
  bufferCastato[0] = bufferStatico[index];
  *retval = nBytes; // numero di caratteri letti = 1
  return 0;
}
