#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <lib.h>
#include <uio.h>
#include <syscall.h>
#include <vnode.h>
#include <vfs.h>
#include <current.h>
#include <synch.h>

int sys__exit(int code) {
  curthread->exitCode = code;
  thread_exit();
  return 0;
}
