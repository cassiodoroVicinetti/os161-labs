#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <lib.h>
#include <uio.h>
#include <syscall.h>
#include <vnode.h>
#include <vfs.h>
#include <current.h>

int sys_write(userptr_t buffer, int nBytes, int *retval) {
  int i;
  for (i = 0; i < nBytes; ++i)
    kprintf("%c", ((char*)buffer)[i]);
  *retval = 1; // numero di caratteri scritti
  return 0;
}
